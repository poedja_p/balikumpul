﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace VoucherApp.Helpers
{
    public class ValidateFileAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            int maxContent = 1024 * 1024 * 5; // 5 MB
            string[] sAllowedExt = new string[] { ".jpg", ".jpeg", ".gif", ".png" };


            var file = value as HttpPostedFileBase;

            if (file == null)
                return false;
            else if (!sAllowedExt.Contains(file.FileName.Substring(file.FileName.LastIndexOf('.')).ToLowerInvariant()))
            {
                ErrorMessage = "Format foto anda salah, format yang diperbolehkan: " + string.Join(", ", sAllowedExt);
                return false;
            }
            else if (file.ContentLength > maxContent)
            {
                ErrorMessage = "Foto anda terlalu besar, ukuran maksimum foto yang diperbolehkan: " + (maxContent / 1024).ToString() + "MB";
                return false;
            }
            else
                return true;
        }
    }

    public class ValidateCSVFileAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            string[] sAllowedExt = new string[] { ".csv" };


            var file = value as HttpPostedFileBase;

            if (file == null)
                return false;
            else if (!sAllowedExt.Contains(file.FileName.Substring(file.FileName.LastIndexOf('.')).ToLowerInvariant()))
            {
                ErrorMessage = "Format file anda salah, format yang diperbolehkan: " + string.Join(", ", sAllowedExt);
                return false;
            }
            else
                return true;
        }
    }
}