﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using VoucherApp.Helpers;

namespace VoucherApp.Models
{
    public class RegistrantsViewModel
    {
        public ObservableCollection<VoucherRegistrant> Registrants { get; set; }
        public int AvailableVoucherCount { get; set; }

        [Required(ErrorMessage = "Csv file can't be empty")]
        [DataType(DataType.Upload)]
        [ValidateCSVFile]
        [Display(Name = "Upload CSV file for new vouchers")]
        public HttpPostedFileBase CsvUpload { get; set; }

        public string ErrorMessage { get; set; }
    }
}