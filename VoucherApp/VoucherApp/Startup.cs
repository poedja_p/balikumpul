﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(VoucherApp.Startup))]
namespace VoucherApp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
